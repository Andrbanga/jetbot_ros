/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ultrasound_goal_client.cpp                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: Bogoslovskiy A <addlesssugar@gmail.com>    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/12/23 15:21:05 by Bogoslovski       #+#    #+#             */
/*   Updated: 2019/12/25 13:02:24 by Bogoslovski      ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <ros/ros.h>
#include <jetbot_use/NavGoal.h>

int main(int argc, char **argv)
{
	ros::init(argc, argv, "ultrasound_goal_client");
	ros::NodeHandle n;

	ros::ServiceClient client = n.serviceClient<jetbot_use::NavGoal>("NavGoal");

	jetbot_use::NavGoal goal_req;

	double buf;

	//square_circling

	n.param("/ultrasound_goal_client/point_1_x", buf, 0.0);
	goal_req.request.x = buf;
	n.param("/ultrasound_goal_client/point_1_y", buf, 0.0);
	goal_req.request.y = buf;
	client.call(goal_req);

	n.param("/ultrasound_goal_client/point_2_x", buf, 0.0);
	goal_req.request.x = buf;
	n.param("/ultrasound_goal_client/point_2_y", buf, 0.0);
	goal_req.request.y = buf;
	client.call(goal_req);

	n.param("/ultrasound_goal_client/point_3_x", buf, 0.0);
	goal_req.request.x = buf;
	n.param("/ultrasound_goal_client/point_3_y", buf, 0.0);
	goal_req.request.y = buf;
	client.call(goal_req);

	n.param("/ultrasound_goal_client/point_4_x", buf, 0.0);
	goal_req.request.x = buf;
	n.param("/ultrasound_goal_client/point_4_y", buf, 0.0);
	goal_req.request.y = buf;
	client.call(goal_req);

	return 0;
}
