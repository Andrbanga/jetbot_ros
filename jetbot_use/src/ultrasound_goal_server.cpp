/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ultrasound_goal_server.cpp                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: Bogoslovskiy A <addlesssugar@gmail.com>    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/12/20 17:19:02 by Bogoslovski       #+#    #+#             */
/*   Updated: 2019/12/25 13:14:47 by Bogoslovski      ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

//ROS architecture
#include <ros/ros.h>

//Marvelmind includes
#include <marvelmind_nav/beacon_distance.h>
#include <marvelmind_nav/hedge_imu_raw.h>
#include <marvelmind_nav/hedge_pos.h>
#include <marvelmind_nav/beacon_pos_a.h>
#include <marvelmind_nav/hedge_pos_ang.h>

#include <jetbot_use/NavGoal.h>

#include <geometry_msgs/Twist.h>

#include <math.h>
#include <cmath>
#include <ctgmath>

#define HEDGE_UPD_RATE 10 //in hz
#define HEDGE_TIME_DEADLINE 5 //in sec
#define PI 3.14159265

class FollowGoalPoint
{
	ros::NodeHandle n;
	ros::ServiceServer service;
	ros::Subscriber hedge_sub;
	ros::Publisher cmd_vel;

	bool goal_reached;
	float x_curr, y_curr, a_curr;
	float x_goal, y_goal;

	ros::Time hedge_callback_time;

	public:
	FollowGoalPoint()
	{
		service = n.advertiseService("NavGoal", &FollowGoalPoint::NavGoalRequestCallback, this);
		cmd_vel = n.advertise<geometry_msgs::Twist>("/cmd_vel", 10);
		ROS_INFO("ultrasound_goal_server ready");
		ros::
	}

	bool	NavGoalRequestCallback(jetbot_use::NavGoal::Request& request, jetbot_use::NavGoal::Response& response)
	{
		ROS_INFO("NavGoalRequestCallback");

		goal_reached = false;

		x_goal = request.x;
		y_goal = request.y;

		if (main_logic())
		{
			ROS_INFO("The goal is reached");
			response.x_res = x_curr;
			response.y_res = y_curr;
			response.result_status = "The goal is reached";
			return true;
		}
		else
		{
			ROS_INFO("Error occurred, the goal is not reached");
			response.result_status = "Error occurred, the goal is not reached";
			return false;
		}
	}

	int		main_logic()
	{
		hedge_sub = n.subscribe("/hedge_pos_ang", 100, &FollowGoalPoint::HedgeMsgCallback, this);
		hedge_callback_time = ros::Time::now();

		ros::Rate r(HEDGE_UPD_RATE);

		while (ros::ok() && !goal_reached)
		{
			if ((ros::Time::now().toSec() - hedge_callback_time.toSec()) > HEDGE_TIME_DEADLINE)
				return (0);
			ros::spinOnce();
			r.sleep();
		}
		ROS_INFO("Reached goal point");
		return (1);
	}

	void	HedgeMsgCallback(const marvelmind_nav::hedge_pos_ang::ConstPtr& msg)
	{
		float x_dist, y_dist, dist, a_goal, a_delta;

		hedge_callback_time = ros::Time::now();

		x_curr = msg->x_m;
		y_curr = msg->y_m;
		a_curr = msg->angle;

		x_dist = x_goal - x_curr;
		y_dist = y_goal - y_curr;
		dist = sqrt(pow(x_dist, 2) + pow(y_dist, 2));

		if (dist < 0.1)
		{
			goal_reached = true;
			return ;
		}

		a_goal = atan2(y_dist, x_dist) * 180 / PI;

		a_delta = a_goal - a_curr;
		if (abs(a_delta) < 15)
			send_speeds(1, 0);
		else if (a_delta >= 15)
			send_speeds(0, 1);
		else if (a_delta <= 15)
			send_speeds(0, -1);
	}

	void	send_speeds(int v_forward, int v_ang)
	{
		geometry_msgs::Twist vel_msg;

		if (v_forward)
			vel_msg.linear.x = 0.3;
		if (v_ang == 1)
			vel_msg.angular.z = 0.3;
		else if (v_ang == -1)
			vel_msg.angular.z = -0.3;

		cmd_vel.publish(vel_msg);
	}
};

int main(int argc, char **argv)
{
	ros::init(argc, argv, "ultrasound_goal_server");
	FollowGoalPoint go;
	return (0);
}
